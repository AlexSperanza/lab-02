package lab.Es4

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

import lab.Es4.Currying.{curriedLambda, nonCurriedLambda, curriedMethod, nonCurriedMethod}

class CurryingTest {

  @Test
  def testCurriedLambda(): Unit = {
    assertTrue(curriedLambda(3)(4)(5))
  }

  @Test
  def testNonCurriedLambda(): Unit = {
    assertTrue(nonCurriedLambda(3, 4, 5))
  }

  @Test
  def testCurriedMethod(): Unit = {
    assertTrue(curriedMethod(3)(4)(5))
  }

  @Test
  def testNonCurriedMethod(): Unit = {
    assertTrue(nonCurriedMethod(3, 4, 5))
  }
}
