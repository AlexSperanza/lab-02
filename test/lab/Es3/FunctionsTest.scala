package lab.Es3

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import lab.Es3.Functions.{lambdaNeg, parity, neg}

class FunctionsTest {

  val lambdaParity: (Int) => String = lab.Es3.Functions.lambdaParity

  @Test
  def testParity() {
    assertEquals("odd", parity(3))
    assertEquals("even", parity(2))
  }

  @Test
  def testLambdaParity() {
    assertEquals("odd", lambdaParity(3))
    assertEquals("even", lambdaParity(2))
  }

  @Test
  def testNeg() {
    val predicate: String => Boolean = _ == ""
    assertEquals(neg(predicate)("Hello"), !predicate("Hello"))
  }

  @Test
  def testLambdaNeg(): Unit = {
    val predicate: String => Boolean = _ == ""
    assertEquals(lambdaNeg(predicate)("Hello"),!predicate("Hello"))
  }

  @Test
  def testGenericNeg(): Unit = {
    val predicate: Int => Boolean = _ > 0
    assertEquals(neg(predicate)(5), !predicate(5))
  }
}
