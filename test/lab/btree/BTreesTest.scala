package lab.btree

import lab.btree.BTrees._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class BTreesTest {
  val tree = Branch(Branch(Empty(), 10, Empty()),
                    20,
                    Branch(Empty(), 30, Empty()))

  @Test
  def testSum(): Unit = {
    assertEquals(60, sum(tree))
  }

  @Test
  def testCount() {
    assertEquals(3, count(tree))
    assertEquals(0, count(Empty()))
  }
}
