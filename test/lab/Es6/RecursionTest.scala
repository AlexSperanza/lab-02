package lab.Es6

import lab.Es6.Recursion.fib
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RecursionTest {

  @Test
  def testFib(): Unit ={
    assertEquals(5, fib(5))
  }
}
