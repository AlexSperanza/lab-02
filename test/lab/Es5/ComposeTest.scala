package lab.Es5

import lab.Es5.Compose.{compose, composeGeneric}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ComposeTest {
  @Test
  def testIntCompose(): Unit = {
    val f: Int => Int = _ + 1
    val g: Int => Int = _ * 2
    assertEquals(11, compose(f, g)(5))
  }

  @Test
  def testGenericCompose(): Unit = {
    val f: Double => Long = _.round
    val g: Int => Double = _ / 100.0
    assertEquals(5, composeGeneric(f, g)(459))
  }

  @Test
  def testGenericComposeWithSubType(): Unit = {
    val f: Long => String = x => x match {
      case x if x % 2 == 0 => "even"
      case _ => "odd"
    }
    val g: Double => Long = _.round

    assertEquals("even", composeGeneric(f, g)(6.24))
  }
}
