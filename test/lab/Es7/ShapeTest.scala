package lab.Es7

import lab.Es7.Shape.{Circle, Rectangle, Shape, Square, area, perimeter}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ShapeTest {

  @Test
  def testRectanglePerimeter(): Unit = {
    val rectangle: Shape = Rectangle(4, 5)
    assertEquals(18, perimeter(rectangle))
  }

  @Test
  def testCirclePerimeter(): Unit = {
    val circle: Shape = Circle(10)
    assertEquals(Math.PI * 20, perimeter(circle))
  }

  @Test
  def testSquarePerimeter(): Unit = {
    val square: Shape = Square(5)
    assertEquals(20, perimeter(square))
  }

  @Test
  def testRectangleArea(): Unit = {
    val rectangle: Shape = Rectangle(4, 5)
    assertEquals(20, area(rectangle))
  }

  @Test
  def testCircleArea(): Unit = {
    val circle: Shape = Circle(10)
    assertEquals(Math.PI * 100, area(circle))
  }

  @Test
  def testSquareArea(): Unit = {
    val square: Shape = Square(5)
    assertEquals(25, area(square))
  }
}
