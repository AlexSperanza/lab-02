package lab.Es8

import org.junit.jupiter.api.Test
import lab.Es8.Optionals.Option
import lab.Es8.Optionals.Option.{filter, map, map2}
import org.junit.jupiter.api.Assertions.assertEquals

class OptionalExtensionTest {

  @Test
  def testFilter(): Unit = {
    val v: Option[Int] = Option.Some(5)
    assertEquals(v, filter(v)(_ > 0))
  }

  @Test
  def testMap(): Unit = {
    val o: Option[Int] = Option.Some(-5)
    val r: Option[Boolean] = Option.Some(false)
    assertEquals(r, map(o)(_ >= 0))
  }

  @Test
  def testMap2(): Unit = {
    val o1: Option[Int] = Option.Some(4)
    val o2: Option[Int] = Option.Some(6)
    val r: Option[Int] = Option.Some(10)
    assertEquals(r, map2(o1, o2)(_ + _))
  }
}
