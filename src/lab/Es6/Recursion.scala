package lab.Es6

object Recursion {

  // It is not a tail recursion because after the recursive calls it needs to calculate the sum
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
  }

}
