package lab.Es4

object Currying {
  val curriedLambda: (Int => (Int => (Int => Boolean))) = x => (y => (z => (x <= y && y <= z)))
  val nonCurriedLambda: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  def curriedMethod(x: Int): Int => (Int => Boolean) = {
    y => (z => x <= y && y <= z)
  }

  def nonCurriedMethod(x: Int, y: Int, z: Int): Boolean = {
    x <= y && y <= z
  }
}
