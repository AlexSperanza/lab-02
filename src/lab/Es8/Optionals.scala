package lab.Es8

object Optionals {

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt: Option[A]): ((A => Boolean) => Option[A]) = opt match {
      case Some(x) => (pred => if (pred(x)) {Some(x)} else {None()})
      case None() => (_ => None())
    }

    def map[A, B](opt: Option[A]): ((A => B) => Option[B]) = opt match {
      case Some(x) => (transform => Some(transform(x)))
      case None() => (_ => None())
    }

    def map2[A](opt1: Option[A], opt2: Option[A]): (((A, A) => A) => Option[A]) = (opt1, opt2) match {
      case (Some(x), Some(y)) => (biFunction => Some(biFunction(x, y)))
      case _ => (_ => None())
    }
  }
}
