package lab.Es5

object Compose {
  def compose(f: Int => Int, g: Int => Int): (Int => Int) = {
    x => f(g(x))
  }

  def composeGeneric[A, B <: C, C, D](f: C => D, g: A => B): (A => D) = {
    x => f(g(x))
  }
}
