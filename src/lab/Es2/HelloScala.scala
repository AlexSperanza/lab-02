package lab.Es2

object HelloScala extends App {
  def printHello(): Unit = {
    println("Hello, Scala!")
  }

  printHello()
}
