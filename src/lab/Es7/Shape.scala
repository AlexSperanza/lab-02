package lab.Es7

object Shape {
  sealed trait Shape
  case class Rectangle(width: Double, height: Double) extends Shape
  case class Circle(radius: Double) extends Shape
  case class Square(side: Double) extends Shape

  def perimeter(shape: Shape): Double = shape match {
    case Rectangle(width, height) => width * 2 + height * 2
    case Circle(radius) => radius * 2 * math.Pi
    case Square(side) => side * 4
  }

  def area(shape: Shape): Double = shape match {
    case Rectangle(width, height) => width * height
    case Circle(radius) => math.Pi * radius * radius
    case Square(side) => side * side
  }
}
