package lab.Es3

object Functions {

  def parity(x: Int): String = x match {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  val lambdaParity: Int => String = x => x match {
    case x if x % 2 == 1 => "odd"
    case _ => "even"
  }

  def neg[A](predicate: A => Boolean): (A => Boolean) = {
    !predicate(_)
  }

  val lambdaNeg: (String => Boolean) => (String => Boolean) = pred => !pred(_)

}
